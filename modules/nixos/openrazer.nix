{ lib, config, ... }:

{
  options = {
    vildravn.openrazer.users = lib.mkOption {
      default = [ ];
      type = lib.types.listOf lib.types.str;
    };
  };

  config = {
    hardware.openrazer = {
      enable = true;
      users = config.vildravn.openrazer.users;
    };
  };  
}
