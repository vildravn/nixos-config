{ pkgs, ... }:

{
  programs.xwayland = {
    enable = true;
  };

  programs.partition-manager.enable = true;

  services.xserver = {
    enable = true;

    displayManager.sddm = {
        enable = true;
        autoNumlock = true;
    };
    
    desktopManager.plasma6 = {
        enable = true;
    };
  };

  environment.systemPackages = with pkgs; [
    kdePackages.discover
  ];
}
