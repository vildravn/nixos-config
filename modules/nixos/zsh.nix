{ pkgs, ... }:

{
  programs.zsh = {
    enable = true;
    ohMyZsh.enable = true;
  };

  # Starship prompt
  # https://starship.rs/
  programs.starship = {
    enable = true;
    interactiveOnly = true;
  };

  users.defaultUserShell = pkgs.zsh;
}
