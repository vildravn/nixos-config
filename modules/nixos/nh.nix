{ lib, config, ... }:

{
  options = {
    vildravn.nh.flakePath = lib.mkOption {
      default = "";
      type = lib.types.str;
    };
  };

  config = {
    programs.nh = {
      enable = true;
      clean.enable = true;
      clean.extraArgs = "--keep-since 4d --keep 3";
      flake = config.vildravn.nh.flakePath;
    };
  };
}
