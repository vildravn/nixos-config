{ pkgs, ... }:

{
  hardware.steam-hardware = {
    enable = true;
  };
  
  programs.steam = {
    enable = true;

    remotePlay.openFirewall = true;
    localNetworkGameTransfers.openFirewall = true;
  };

  programs.gamemode = {
    enable = true;
  };

  services.input-remapper = {
    enable = true;
  };

  environment.systemPackages = with pkgs; [
    mangohud
    steamtinkerlaunch
    heroic
  ];
}
