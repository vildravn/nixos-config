{ pkgs, ... }:

{
  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };

  environment.systemPackages = with pkgs; [
    kitty
    dunst
    hyprshade
    hypridle
    hyprlock
    #hyprcursor
    hyprkeys
    hyprpaper
    grimblast
    cliphist
    wl-clip-persist
    eww
    wofi
  ];
}
