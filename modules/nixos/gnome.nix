{ pkgs, ... }:

{
  programs.xwayland = {
    enable = true;
  };

  services.xserver = {
    enable = true;

    displayManager.gdm = {
        enable = true;
    };

    desktopManager.gnome = {
        enable = true;
    };
  };

  programs.nautilus-open-any-terminal = {
    enable = true;
    terminal = "wezterm";
  };

  environment.systemPackages = with pkgs; [
    gnome-tweaks
    gnome-software
    
    gnomeExtensions.appindicator
    gnomeExtensions.just-perfection
    gnomeExtensions.fullscreen-avoider
    
    libgda6
    gsound
    gnomeExtensions.pano
  ];
}
