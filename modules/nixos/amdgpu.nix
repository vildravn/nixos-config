{ pkgs, ... }:

{
  programs.corectrl = {
    enable = true;
    gpuOverclock = {
      enable = true;
      ppfeaturemask = "0xffffffff";
    };
  };

  # systemd.user.services.corectrl = {
  #   enable = true;
  #   after = [ "network.target" ];
  #   wantedBy = [ "default.target" ];
  #   description = "Control hardware with ease using application profiles";
  #   serviceConfig = {
  #       Type = "simple";
  #       ExecStart = "${pkgs.corectrl}/bin/corectrl";
  #   };
  # };

  environment.systemPackages = with pkgs; [
    lact
  ];

  systemd.packages = with pkgs; [
    lact
  ];

  systemd.services = {
    lactd .wantedBy = ["multi-user.target"];
  };
}
