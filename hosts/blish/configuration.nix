{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      #../../modules/nixos/plasma.nix
      ../../modules/nixos/gnome.nix
      ../../modules/nixos/pipewire.nix
      ../../modules/nixos/flatpak.nix
      ../../modules/nixos/games.nix
      ../../modules/nixos/openrazer.nix
      ../../modules/nixos/zsh.nix
      ../../modules/nixos/git.nix
      #../../modules/nixos/hyprland.nix
      ../../modules/nixos/nh.nix
      ../../modules/nixos/amdgpu.nix
    ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Define hostname
  networking.hostName = "blish";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set time zone.
  #time.timeZone = "CET";
  services.automatic-timezoned.enable = true;
  services.localtimed.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "sv_SE.UTF-8";
    LC_IDENTIFICATION = "sv_SE.UTF-8";
    LC_MEASUREMENT = "sv_SE.UTF-8";
    LC_MONETARY = "sv_SE.UTF-8";
    LC_NAME = "sv_SE.UTF-8";
    LC_NUMERIC = "sv_SE.UTF-8";
    LC_PAPER = "sv_SE.UTF-8";
    LC_TELEPHONE = "sv_SE.UTF-8";
    LC_TIME = "sv_SE.UTF-8";
  };

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "se";
    variant = "";
  };

  # Configure console keymap
  console.keyMap = "sv-latin1";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vildravn = {
    isNormalUser = true;
    description = "Vildravn";
    extraGroups = [ "networkmanager" "wheel" "gamemode" "corectrl" ];
    packages = with pkgs; [
      firefox
      vscode-fhs
    ];
    useDefaultShell = true;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  virtualisation.podman = {
    enable = true;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    distrobox
    SDL2
    wezterm
    obsidian
  ];

  #services.teamviewer.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  system.stateVersion = "23.11"; # Did you read the comment?

  vildravn.openrazer.users = [ "vildravn" ];

  vildravn.nh.flakePath = "/home/vildravn/nixos-config";

  services.printing.enable = false;

  services.avahi.enable = false;

  programs.appimage = {
    enable = true;
    binfmt = true;
  };

  programs.nix-ld = {
    enable = true;
    libraries = with pkgs; [
      zlib
      zstd
      stdenv.cc.cc
      curl
      openssl
      attr
      libssh
      bzip2
      libxml2
      acl
      libsodium
      util-linux
      xz
      systemd
      SDL2
    ];
  };
}

